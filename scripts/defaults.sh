#!/bin/bash

echo Now running the "defaults.sh" provisioning script.

# Set default values for these vars if they're not already set.
[[ -z ${DEPLOY} ]] && { DEPLOY=release; echo s/@DEPLOY@/release/g >> /vagrant/.work/vars.sed; }
[[ -z ${PROJECT} ]] && { PROJECT=xnat; echo s/@PROJECT@/xnat/g >> /vagrant/.work/vars.sed; }
[[ -z ${SITE} ]] && { SITE=XNAT; echo s/@SITE@/XNAT/g >> /vagrant/.work/vars.sed; }
[[ -z ${XNAT_VERSION} ]] && { XNAT_VERSION=1.7.6; echo s/@XNAT_VERSION@/1.7.6/g >> /vagrant/.work/vars.sed; }
[[ -z ${XNAT_REV} ]] && { XNAT_REV=${XNAT_VERSION}; echo s/@XNAT_REV@/${XNAT_VERSION}/g >> /vagrant/.work/vars.sed; }
[[ -z ${XNAT} ]] && { XNAT=xnat-${XNAT_REV}; echo s/@XNAT@/xnat-${XNAT_REV}/g >> /vagrant/.work/vars.sed; }
[[ -z ${PIPELINE_VERSION} ]] && { PIPELINE_VERSION=1.7.6; echo s/@PIPELINE_VERSION@/1.7.6/g >> /vagrant/.work/vars.sed; }
[[ -z ${PIPELINE_REV} ]] && { PIPELINE_REV=${PIPELINE_VERSION}; echo s/@PIPELINE_REV@/${PIPELINE_VERSION}/g >> /vagrant/.work/vars.sed; }
[[ -z ${PIPELINE_INST} ]] \
    && { PIPELINE_INST=pipeline-installer-${PIPELINE_REV}; \
    echo s/@PIPELINE_INST@/pipeline-installer-${PIPELINE_REV}/g >> /vagrant/.work/vars.sed; }

[[ -z ${SMTP_AUTH} ]] && { SMTP_DELIM=""; } || { SMTP_DELIM=":"; }

# Special case: this isn't used in replace operations, so doesn't go into sed script.
[[ -z ${SILENT_LOAD} ]] && { SILENT_LOAD=true; }

echo Completed processing "defaults.sh" script
