#!/usr/bin/env bash

checkWorkFolder() {
    RESPONSE=""

    [[ -d .work ]] && {
        workExists
    }

    [[ ! -d .work ]] && {
        mkdir -p .work
    }

    unset RESPONSE
}

workExists() {
    echo "The folder .work, which is used to store variables and configurations for use in building and managing your VM,"
    echo "already exists. Do you want to delete this folder and build your VM strictly from the values in your configuration"
    echo "files or re-use the cached configuration information?"

    promptWithExit "Press Y to delete the cache folder, N to keep it, or X to cancel"
    RESPONSE=${?}

    case ${RESPONSE} in
        0)
            echo "Keeping the folder .work..."
            ;;
        1)
            echo "Deleting the folder .work..."
            rm -rf .work
            ;;
        2)
            echo "Exiting..."
            exit 0;
            ;;
    esac
}

askToDeleteWork() {
    local ANSWER=""
    while true; do
        echo
        read -n 1 -s -r -p "Press Y to delete the cache folder, N to keep it, or X to cancel ) " ANSWER
        echo
        [[ ${ANSWER} =~ ^[NnXxYy]$ ]] && {
            RESPONSE=${ANSWER}
            break
        } || {
            echo
            echo "Please enter yes (Y), no (N), or exit (X)."
        }
    done
    echo
}

prompt() {
    local REPROMPT="Please enter yes (Y) or no (N)"

    [[ ${1} == "withExit" ]] && {
        local WITH_EXIT="true"
        REPROMPT="Please enter yes (Y), no (N), or exit (X)."
        shift
    }

    local MESSAGE="${*}"

    local ANSWER=""
    while true; do
        echo
        read -n 1 -s -r -p "${MESSAGE} ) " ANSWER
        echo
        [[ ${ANSWER} =~ ^[Yy]$ ]] && { return 1; }
        [[ ${ANSWER} =~ ^[Nn]$ ]] && { return 0; }
        [[ ${ANSWER} =~ ^[Xx]$ && ! -z ${WITH_EXIT} ]] && { return 2; }
        echo
        echo "${REPROMPT}"
    done
    echo
}

promptWithExit() {
    prompt withExit ${*}
}

