#!/bin/bash
#
# XNAT Vagrant provisioning script
# http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine, all rights reserved.
# Released under the Simplified BSD license.
#

echo "Now running the \"provision.sh\" provisioning script."

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Initialize version info for Tomcat and PostgreSQL
TOMCAT="$(find /var/lib -mindepth 1 -maxdepth 1 -type d -name "tomcat*" | xargs basename)"
case ${TOMCAT} in
    "tomcat7")
        TOMCAT_VERSION="7"
        ;;

    "tomcat8")
        TOMCAT_VERSION="8"
        ;;

    *)
        TOMCAT_VERSION="$(/usr/share/tomcat/bin/version.sh | fgrep "Server number:" | cut -f 2 -d : | tr -d " " | cut -f 1 -d ".")"
        ;;
esac
echo "TOMCAT=\"${TOMCAT}\"" >> /vagrant/.work/vars.sh
echo "TOMCAT_VERSION=\"${TOMCAT_VERSION}\"" >> /vagrant/.work/vars.sh
echo "s/@TOMCAT@/${TOMCAT}/g" >> /vagrant/.work/vars.sed
echo "s/@TOMCAT_VERSION@/${TOMCAT_VERSION}/g" >> /vagrant/.work/vars.sed
echo "DB_VERSION=\"$(find /etc/postgresql -mindepth 1 -maxdepth 1 -type d | xargs basename | sort -n | tail -n 1)\"" >> /vagrant/.work/vars.sh

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript bin/macros
sourceScript defaults.sh

# set 'update_vm: false' to skip guest OS update (which can take a long time)
if [[ ${UPGRADE_VM} != false ]]; then

    # Update the VM.
    ${INSTALL_CMD} update

    # Take an extra step with ubuntu: yum update does the whole thing, but for ubuntu we need to force upgrade to install everything without
    # requiring user interaction, especially grub upgrades.
    [[ ${ID} == "ubuntu" ]] && { ${INSTALL_CMD} -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade; }

fi

# Install any additional specified packages
if [[ -v INSTALL ]]; then
    echo "Installing additional packages: ${INSTALL}"
    ${INSTALL_CMD} -y install ${INSTALL}
fi

# Update security stuff
sudo update-ca-certificates -f

# Create XNAT user
#  1. Create user group
#  2. Create user. Flags:
#       -g initial_group        User's initial login group
#       -G group[,...]          Supplementary groups of which user is a member
#       -d home_dir             User's login directory
#       -m                      Create home_dir if it does not exist
#       -s shell                User's login shell
echo ""

ROOT="$(dirname ${XNAT_HOME})"
sudo [ ! -d ${ROOT} ] && { mkdir -p ${ROOT}; }

USER_ADD_OPTS=""
[[ ! -z ${XNAT_USER_GID} ]] && { groupadd --gid ${XNAT_USER_GID} ${XNAT_USER}; USER_ADD_OPTS=" --gid ${XNAT_USER_GID}"; }
[[ ! -z ${XNAT_USER_UID} ]] && { USER_ADD_OPTS="${USER_ADD_OPTS} --uid ${XNAT_USER_UID}"; }

grep -q -w docker /etc/group
[[ ${?} == 0 ]] && { XNAT_GROUPS="users,vagrant,docker"; } || { XNAT_GROUPS="users,vagrant"; }

if [[ ! -d ${XNAT_HOME} ]]; then
    echo "Creating XNAT user with the home directory ${XNAT_HOME}"
    sudo useradd -G ${XNAT_GROUPS} -d ${XNAT_HOME} -m -s /bin/bash ${USER_ADD_OPTS} ${XNAT_USER}
else
    echo "Creating XNAT user with the existing home directory ${XNAT_HOME}"
    sudo useradd -G ${XNAT_GROUPS} -d ${XNAT_HOME} -M -s /bin/bash ${USER_ADD_OPTS} ${XNAT_USER}
fi

# Create the various home folders and properties files if they don't already exist.
createUserHome

if [[ ! -z ${XNAT_PASS} ]]; then
    if [[ ${XNAT_PASS} == "default" ]]; then
        sudo $(echo "${XNAT_USER}:${XNAT_USER}" | chpasswd)
    else
        sudo $(echo "${XNAT_USER}:${XNAT_PASS}" | chpasswd)
    fi
else
    echo XNAT_PASS not set or empty, leaving password unset, ssh public-private key access only.
fi

# Create the VM user's bash profile.
if [[ ! -e ${XNAT_HOME}/.bash_profile ]]; then
    echo ""
    echo "Creating XNAT user's bash profile"
    tokenizeConfig bash.profile ${XNAT_HOME}/.bash_profile
fi

# Set up ssh keys for VM user.
if [[ ! -e ${XNAT_HOME}/.ssh ]]; then
    echo ""
    echo "Copying vagrant ssh keys"
    sudo cp -R /home/vagrant/.ssh ${XNAT_HOME}
fi

# Now make anything non-XNAT_USER-y XNAT_USER-y.
setFolderOwner ${XNAT_USER} ${XNAT_HOME}

# Add VM user to list of NOPASSWD sudoers.
echo ""
echo "Adding XNAT user to list of NOPASSWD sudoers"
tokenizeConfig sudoers.d /etc/sudoers.d/${XNAT_USER}

## Set up Docker to listen for external connections
#echo ""
#echo "Creating Docker service configuration file"
#sudo mkdir /etc/systemd/system/docker.service.d
#tokenizeConfig docker.conf /etc/systemd/system/docker.service.d/docker.conf

# Setup Docker RemoteAPI
echo ""
echo "Opening access to port 2375 and docker.sock for Docker RemoteAPI"
echo "DOCKER_OPTS='-H tcp://0.0.0.0:2375 -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock'" | sudo tee -a /etc/default/docker

echo ""
echo "Disabling the ${TOMCAT} service during restart and provisioning."
toggleService ${TOMCAT} off

echo ""
echo "Configuring the chronyd service..."
[[ $(systemctl | fgrep chrony | wc -l) == 0 ]] && { ${INSTALL_CMD} -y install chrony; }
manageService chrony restart
[[ -n ${TIMEZONE} ]] && { sudo timedatectl set-timezone ${TIMEZONE}; }

echo "Completed initial provisioning, the VM should now restart to finalize its configuration"

