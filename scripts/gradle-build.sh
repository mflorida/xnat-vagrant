#!/bin/bash

#
# XNAT download and installation
#

# Suppress decorated or formatted output from gradle.
TERM=dumb

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript defaults.sh
sourceScript bin/macros

if [[ -e ~/.bash_profile ]]; then
    source ~/.bash_profile;
elif [[ -e ~/.bashrc ]]; then
    source ~/.bashrc;
fi

# We're really starting this script now.
echo Now running the "gradle-build.sh" provisioning script.

# Stop these services so that we can configure them later.
manageService ${TOMCAT} stop
manageService nginx stop

# Make SURE Tomcat is shut down.
TOMCAT_PID=$(ps ax | fgrep ${TOMCAT} | fgrep org.apache.catalina.startup.Bootstrap | awk '{$1=$1};1' | cut -f 1 -d " ")
[[ -n ${TOMCAT_PID} ]] && { sudo kill -9 ${TOMCAT_PID}; sudo rm /var/run/${TOMCAT}.pid; }

# Configure the host settings.
echo -e "${VM_IP} ${HOST} ${SERVER}" | sudo tee --append /etc/hosts > /dev/null

configureNginx

echo "Starting nginx..."
manageService nginx start

# TOMCAT STUFF
CURRENT_USER=$(whoami)
setFolderOwner ${CURRENT_USER} /var/lib/${TOMCAT}

setTomcatConfiguration

# Remove any existing web apps
rm -rf /var/lib/${TOMCAT}/webapps/*

# Set up PostgreSQL
configurePostgreSQL

# XNAT STUFF

# setup XNAT data folders
setupFolders ${DATA_ROOT} ${CURRENT_USER}

# Copies or downloads specified release archive or folder
downloadSrc() {

    SRC=$1

    echo "Downloading ${SRC} to $(pwd)"
    wget -N -q -P ${DATA_ROOT}/src ${SRC} || echo "Error downloading '${SRC}'"

}

# clone dev source repo or copy a repo (folder) already downloaded locally
getDev() {

    SRC=$1
    BCH=$2
    DIR=$3
    COPIED=1

    SRC_DIR=${DATA_ROOT}/src/${DIR}

    # check to see if the src dir already exists
    if [[ -e ${SRC_DIR} ]]; then
        echo "The source repository already exists: ${SRC_DIR}"
        COPIED=0
    # clone with Git if SRC ends with .git
    elif [[ ${SRC} == *.git ]]; then
        echo "Cloning branch/tag '${BCH}' from '${SRC}' to '${SRC_DIR}'"
        # git clone --branch <tag_name> <repo_url> <dest_dir>
        git clone --branch ${BCH} ${SRC} ${SRC_DIR}
        COPIED=$?
    else
        echo "Downloading ${SRC}"
        downloadSrc ${SRC}
        COPIED=$?
    fi

    if [[ ${COPIED} != 0 ]]; then
        copyLocal ${SRC} ${SRC_DIR}
        COPIED=$?
        if [[ ${COPIED} != 0 ]]; then
            echo "Copy failed."
            exit;
        fi
    fi

    # in case the 'dev' source is an archive of the repo
#    echo Uncompressing ${SRC##*/}
    uncompress ${SRC##*/}
}

# get dev or release files
getDev ${XNAT_SRC} ${XNAT_BRANCH} ${XNAT};
getDev ${PIPELINE_SRC} ${PIPELINE_BRANCH} ${PIPELINE_INST};

PIPE_DIR=${PIPELINE_SRC##*/}; PIPE_DIR=${PIPE_DIR%.tar.gz}; PIPE_DIR=${PIPE_DIR%.zip};

if [[ -d ${SRC_DIR} ]]; then
    replaceTokens pipeline.gradle.properties > ${SRC_DIR}/gradle.properties
    pushd ${SRC_DIR}
    echo Installing pipeline engine from ${SRC_DIR} to ${PIPE_DIR}
    sudo ./gradlew -q
    echo Pipeline engine installation completed.
    popd
fi

# Is the variable MODULES defined?
[[ -v MODULES ]] \
    && { echo Found MODULES set to ${MODULES}, pulling repositories.; /vagrant-root/scripts/pull_module_repos.rb ${DATA_ROOT}/modules ${MODULES}; } \
    || { echo No value set for the MODULES configuration, no custom functionality will be included.; }

# Gradle deployment setup
timestamp=$(date +%s);
mkdir -p ${XNAT_HOME}/.gradle
cd ${XNAT_HOME}/.gradle
[[ -e gradle.properties ]] && { mv gradle.properties gradle-${timestamp}.properties; }
echo "# ${XNAT_HOME}/.gradle/gradle.properties" > gradle.properties
echo archiveName=${CONTEXT} >> gradle.properties
echo tomcatHome=/var/lib/${TOMCAT} >> gradle.properties
cd -

# Search for any post-build execution folders and execute the install.sh
for POST_DIR in /vagrant/post_*; do
    if [[ -e ${POST_DIR}/install.sh ]]; then
        echo Executing post-processing script ${POST_DIR}/install.sh
        bash ${POST_DIR}/install.sh
    fi
done

sudo rm -rf /var/log/${TOMCAT}/*

# optionally run the Gradle build inside the VM
if [[ ! -z ${DEPLOY} && ${DEPLOY} == gradle-vm ]]; then
    echo "Starting Gradle build..."
    cd ${DATA_ROOT}/src/${XNAT}
    sudo ./gradlew war -q --info && { [ -e build/libs/xnat-web-*.war ] && { echo "Gradle build completed, deploying to Tomcat."; cp build/libs/xnat-web-*.war /var/lib/${TOMCAT}/webapps/${CONTEXT}.war; fixContext /var/lib/${TOMCAT}/webapps/${CONTEXT}.war; } || { echo Ran the Gradle build, but there\'s no xnat-web war file in build/libs.; } } || die "Gradle build failed."
    cd -
elif [[ ! -z ${CONFIG} && ${CONFIG} == *dev* ]] || [[ ! -z ${DEPLOY} && ${DEPLOY} == war ]]; then
    deployWar
fi

# Lastly, see if there's a public key configured.
if [[ ! -z ${PUBLIC_KEY} ]]; then
    # If so, add it to both Vagrant and XNAT users
    echo Adding public key to authorized keys for ${CURRENT_USER} and ${XNAT_USER}
    addAuthorizedPublicKey ~/.ssh/authorized_keys
    addAuthorizedPublicKey ${XNAT_HOME}/.ssh/authorized_keys
fi

# Set ownership of data and Tomcat folders to the XNAT user.
setFolderOwner ${XNAT_USER} /data ${DATA_ROOT} /var/lib/${TOMCAT}

# Run any exec scripts the user's added.
[[ -d /vagrant/exec ]] && {
    for SCRIPT in $(find /vagrant/exec -type f -executable); do
        ${SCRIPT}
    done
}

sudo chmod 755 /var/log/${TOMCAT}

checkForExtensionScripts

# Skip Tomcat startup and quit early if no war file was deployed.
if [[ ! -e /var/lib/${TOMCAT}/webapps/${CONTEXT}.war ]]; then
    printf "reload" > /vagrant/.work/startup
    echo "==========================================================="
    echo "VM is ready. Please deploy XNAT war and start Tomcat."
    echo "Afterwards, your XNAT server will be available at: "
    echo "${SITE_URL}"
    echo "==========================================================="
    exit 0;
fi

echo "Enabling and starting Tomcat..."
toggleService ${TOMCAT} on
startTomcat
monitorTomcatStartup

STATUS=$?
if [[ ${STATUS} == 0 ]]; then
    # after setup is successful, specify 'reload' as the startup command
    printf "reload" > /vagrant/.work/startup
    echo "==========================================================="
    echo "Your VM's IP address is ${VM_IP}. "
    echo "Your XNAT server will be available at: "
    echo "${SITE_URL}"
    echo "==========================================================="
else
    echo The application does not appear to have started properly. Status code: ${STATUS}
    echo The last lines in the log are:; tail -n 40 /var/log/${TOMCAT}/catalina.out;
fi

exit ${STATUS}
