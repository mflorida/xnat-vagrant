#!/usr/bin/env ruby
# convert YAML properties to vars for setup scripts

require 'yaml'
require 'fileutils'

# pass the config directory as the first argument when executing this script
# ./yaml_vars.rb ../configs/foo
cfg_name = File.basename(Dir.getwd)
cfg_dir ||= ARGV[0] || cfg_name
cwd     ||= File.dirname(File.expand_path(cfg_name))

Dir.mkdir("#{cwd}/.work") unless File.exists?("#{cwd}/.work")

# load config settings
puts "Loading #{cwd}/config.yaml"
profile ||= YAML.load_file("#{cwd}/config.yaml")

# load common overrides from configs/global.yaml
global_config = "#{cwd}/../../configs/global.yaml"
if File.exists?(global_config)
    puts "Loading global overrides from #{global_config}..."
    global_yaml = YAML.load_file(global_config)
    global_yaml.each { |k, v|
        profile[k] = v
    }
end

# load local customizations
# (We really want to read these last, but we need to read the config file from here.)
local_path = "#{cwd}/local.yaml"
if File.exists?(local_path)
    puts "Loading local overrides from #{local_path}..."
    local = YAML.load_file(local_path)
    local.each { |k, v|
        profile[k] = v
    }
end

# set name to the folder name
profile['name'] ||= cfg_name != 'scripts' ? cfg_name : profile['name']

# setup some fallback defaults - some of these are for backwards compatibility
profile['xnat'] ||= 'xnat'
profile['verbose'] ||= false
profile['public_key'] ||= ''
profile['context'] ||= 'ROOT'
profile['project'] ||= profile['name']
profile['host'] ||= profile['name']
profile['data_root'] ||= "/data/#{profile['project']}"
profile['admin'] ||= "admin@#{profile['server']}"
profile['xnat_user'] ||= profile['project']
profile['xnat_pass'] ||= ''
profile['xnat_home'] ||= "#{profile['data_root']}/home"
profile['home_pkg'] ||= ''
profile['server'] ||= profile['vm_ip']
profile['xnat_version'] ||= profile['xnat_rev'] ||= ''
profile['xnat_rev'] ||= profile['xnat_version']
profile['pipeline_version'] ||= profile['pipeline_rev'] ||= profile['xnat_version'] ||= ''
profile['pipeline_rev'] ||= profile['pipeline_version']
profile['branch'] ||= 'master'
profile['xnat_branch'] ||= profile['branch']
profile['pipeline_branch'] ||= profile['branch']
profile['pipeline_inst'] ||= 'pipeline'

profile['config'] ||= cfg_dir ||= ''
profile['provision'] ||= ''
profile['build'] ||= ''

if profile['host'] == ''
    profile['host'] = profile['name']
end

if profile['server'] == ''
    profile['server'] = profile['vm_ip']
end

# build siteUrl setting from protocol and server
profile['site_url'] ||= "#{profile['protocol']}://#{profile['server']}#{profile['context'] == 'ROOT' ? '' : '/' + profile['context']}"

profile['protocol'] ||= 'http'
is_secure_protocol  = !(profile['protocol'] !~ /^https/)
is_secure_site_url  = !(profile['site_url'] !~ /^https/)
if !is_secure_site_url && is_secure_protocol
  puts "The specified site protocol is https, but you've specified an insecure site URL: #{profile['site_url']}"
  puts "I've converted the protocol to http to match the configured site URL. If you want to run this XNAT"
  puts "deployment with https, make sure you set both the URL and protocol to use 'https'."
  puts
  profile['protocol'] = 'http'
  is_secure_protocol = false
elsif is_secure_site_url && !is_secure_protocol
  puts "The specified site protocol is http, but you've specified a secure site URL: #{profile['site_url']}"
  puts "I've converted the site URL to use http to match the configured protocol: #{profile['site_url']}"
  puts "If you want to run this XNAT deployment with https, make sure you set both the URL and protocol"
  puts "to use 'https'."
  puts
  profile['site_url'].sub! 'https:', 'http:'
  is_secure_site_url = false
end

if is_secure_protocol && is_secure_site_url
  profile['ssl_cert'] ||= ''
  profile['ssl_key']  ||= ''

  # Validate SSL cert and key settings if present...
  has_cert   = !profile['ssl_cert'].to_s.empty?
  has_key    = !profile['ssl_key'].to_s.empty?
  found_cert = has_cert && File.exists?(profile['ssl_cert'])
  found_key  = has_key && File.exists?(profile['ssl_key'])
  if found_cert && found_key
    cache_dir = "#{cwd}/../../local/#{profile['name']}"
    Dir.mkdir(cache_dir) unless File.exists?(cache_dir)
    FileUtils.cp(profile['ssl_cert'], cache_dir)
    FileUtils.cp(profile['ssl_key'], cache_dir)
    profile['ssl_cert'] = "/vagrant-root/local/#{profile['name']}/#{File.basename(profile['ssl_cert'])}"
    profile['ssl_key']  = "/vagrant-root/local/#{profile['name']}/#{File.basename(profile['ssl_key'])}"
  else
    if has_cert && has_key && !found_cert && !found_key
      puts "You specified an SSL certificate and key to be installed for your XNAT application, but I can't "
      puts "find the specified files #{profile['ssl_cert']} and #{profile['ssl_key']}."
    elsif has_cert && has_key && !found_cert
      puts "You specified an SSL certificate and key to be installed for your XNAT application, but I can't "
      puts "find the specified file #{profile['ssl_cert']}."
    elsif has_cert && has_key && !found_key
      puts "You specified an SSL certificate and key to be installed for your XNAT application, but I can't "
      puts "find the specified file #{profile['ssl_key']}."
    elsif has_cert && !has_key
      puts "You've specified an SSL certificate but no SSL key. You must specify both. Generating default"
      puts "self-signed certificate and key, but you may want to try again."
    elsif !has_cert && has_key
      puts "You've specified an SSL key but no SSL certificate. You must specify both. Generating default"
      puts "self-signed certificate and key, but you may want to try again."
    else
      puts "You have configured XNAT Vagrant to use HTTPS but haven't specified an SSL certificate and key,"
      puts "so a self-signed certificate and key will be generated and installed. You can specify your own"
      puts "certificate and key by setting the 'ssl_cert' and 'ssl_key' properties in local.yaml."
    end
    puts
    profile['ssl_cert'] = ''
    profile['ssl_key']  = ''
    if profile['ssl_subject'].to_s.strip.empty?
      profile['ssl_subject'] = "/C=US/ST=Missouri/L=Saint Louis/O=Washington University School of Medicine/OU=Neuroinformatics Research Group/CN=#{profile['server']}"
      puts "No subject was specified for self-signed SSL certificate generation, so the default value will be"
      puts "used. You can provide a subject value by setting the 'ssl_subject' property in your local.yaml"
      puts "configuration. Find out about subjects here: https://knowledge.digicert.com/solution/SO18140.html#Subject\n"
    end
  end
else
  puts "You've specified the http protocol and a site URL that uses http. No SSL certificate will be generated or installed."
end

if profile['xnat_url'] || profile['xnat_repo']
    profile['xnat_src'] = profile['xnat_url'] ||= profile['xnat_repo']
end

if profile['pipeline_url'] || profile['pipeline_repo']
    profile['pipeline_src'] = profile['pipeline_url'] ||= profile['pipeline_repo']
end

shares = profile['shares'] ||= profile['shared'] ||= profile['share'] ||= ''
has_shares = shares && shares != 'false' && shares != ''

# If there's a setting for public key...
if profile['public_key']
    # Try to find that file
    if File.exists?(profile['public_key'])
        # If it exists, get it and just make sure it's not empty.
        keys = IO.readlines(profile['public_key'])
        if keys.length > 0
            puts "Retrieved public key from file #{profile['public_key']}"
            profile['public_key'] = keys[0].delete!("\n")
        else
            puts "Found the specified public key file at #{profile['public_key']}, but it appears to be empty."
            profile['public_key'] = ''
        end
    else
        puts "Public key file specified as \"#{profile['public_key']}\", but I can't find that."
        profile['public_key'] = ''
    end
end

# tolerate old settings for upgrading the VM OS and packages
profile['update_vm'] ||= profile['upgrade_vm'] ||= profile['update_os'] ||= profile['upgrade_os'] ||= false

# tolerate old settings for updating VM guest additions
profile['update_vbguest'] ||= profile['update_guest'] ||= true

# write to '.work/vars.yaml' file to serve as a Single Point of Truth after setup
puts "Initializing #{cwd}/.work/vars.yaml"
File.open("#{cwd}/.work/vars.yaml", 'wb') { |vars|
    vars.truncate(0)
    vars.puts("# vars.yaml\n")
    vars.puts("# DO NOT EDIT THE CONTENTS OF THIS FILE.\n")
    vars.puts("# IT IS AUTOMATICALLY GENERATED ON SETUP.\n")
    # leverage the .to_yaml method
    vars.puts profile.to_yaml
}

puts "Initializing #{cwd}/.work/vars.sh"
File.open("#{cwd}/.work/vars.sh", 'wb') { |vars|
    vars.truncate(0)
    vars.puts("#!/usr/bin/env bash\n")
    profile.each { |k, v|
        if has_shares && %w[shares shared share].include?(k)
            if profile['verbose']
                puts " * #{k.upcase}='#{v}'"
            end
            vars.puts "#{k.upcase}='#{v}'"
        else
            # empty values get changed to ""
            if profile['verbose']
                puts " * #{k.upcase}=" + (v == '' ? '""' : "\"#{v}\"")
            end
            vars.puts "#{k.upcase}=" + (v == '' ? '""' : "\"#{v}\"")
        end
    }
}

puts "Initializing #{cwd}/.work/vars.sed"
File.open("#{cwd}/.work/vars.sed", 'wb') { |vars|
    vars.truncate(0)
    profile.each { |k, v|
        # exclude shares setting
        unless %w[shares shared share].include?(k)
            _v = "#{v}"
            if profile['verbose']
                puts " * s/@#{k.upcase}@/#{_v.gsub('/', "\\/")}/g"
            end
            vars.puts "s/@#{k.upcase}@/#{_v.gsub('/', "\\/")}/g"
        end
    }
}
