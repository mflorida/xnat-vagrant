filebeat.inputs:

- type: log
  enabled: true
  paths:
    - @XNAT_HOME@/logs/*.log
  multiline.pattern: '^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3} '
  multiline.negate: true
  multiline.match: after

- type: log
  enabled: true
  paths:
    - /var/log/@TOMCAT@/*
  exclude_files: ['\.gz$']
  multiline.pattern: '^\d{2}-[A-z]{3,5}-\d{4} \d{2}:\d{2}:\d{2}\.\d{3} [A-Z]+ '
  multiline.negate: true
  multiline.match: after

#============================= Filebeat modules ===============================

filebeat.config.modules:

  path: ${path.config}/modules.d/*.yml
  reload.enabled: true
  reload.period: 10s

#==================== Elasticsearch template setting ==========================

setup.template.settings:

  index.number_of_shards: 1

#============================== Kibana =====================================

setup.kibana:

  host: "@KIBANA_HOST@"
  protocol: "@KIBANA_PROTOCOL@"
  username: "@KIBANA_USERNAME@"
  password: "@KIBANA_PASSWORD@"

#================================ Outputs =====================================

# Configure what output to use when sending the data collected by the beat.

#-------------------------- Elasticsearch output ------------------------------
output.elasticsearch:

  hosts: ["@ELASTIC_HOST@"]
  protocol: "@ELASTIC_PROTOCOL@"
  username: "@ELASTIC_USERNAME@"
  password: "@ELASTIC_PASSWORD@"

#================================ Logging =====================================

logging.level: info
logging.selectors: ["*"]

#==========================  Modules configuration =============================
filebeat.modules:

  #------------------------------ PostgreSQL Module ------------------------------
  - module: postgresql
    # Logs
    log:
      enabled: true

  #-------------------------------- Nginx Module --------------------------------
  - module: nginx
    # Access logs
    access:
      enabled: true

    # Error logs
    error:
      enabled: true
