#!/bin/bash

source ../../scripts/setup-macros.sh

checkWorkFolder

echo
echo Starting XNAT build...

echo
echo Provisioning VM with specified user...

echo
vagrant up

echo
echo Reloading VM to configure folder sharing...

echo
vagrant reload

echo
echo Running build provision to build and deploy XNAT on the VM...

echo
vagrant provision --provision-with build

echo
echo Provisioning completed.
