XNAT Vagrant
==============================

## About

This Vagrant config will set up the virtual machine to run XNAT but will _not_ install the webapp .war. 
The compiled `.war` file will need to be manually added to the `webapps` folder.
The latest XNAT release war can be found at [download.xnat.org](https://download.xnat.org/).

## Setup

- Run `./run setup` from this folder, or `./run xnat-system setup` from the top-level folder.
- Go get a beverage.
- Start working with XNAT.

### Notes

- This VM is intended for development and trial purposes, and is not designed for
  use in a production environment.
- The default site url for this config will be `http://10.100.100.17`.
- The default (admin) login is username: `admin`, password: `admin`
- You can set a hostname using the `host` property and set a fully qualified domain name
  with the `server` property. These values can be set in a file you create named
  `local.yaml` which will override the settings in `config.yaml`.

## Customization

- To customize your configuration, create a file in this folder named `local.yaml`
  and copy any settings from `config.yaml` and change them to the desired values.
- Refer to the `sample.local.yaml` file for example custom settings.
