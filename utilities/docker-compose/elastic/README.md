# Elasticsearch docker-compose configuration

The Elasticsearch docker-compose configuration provides a couple basic implementations of the [Elasticsearch
application stack](https://www.elastic.co):

* The default configuration in the file [docker-compose.yml](docker-compose.yml) launches a single-node Elasticsearch service with Kibana
* The load-balanced configuration in the file [docker-compose-es-cluster.yml](docker-compose-es-cluster.yml) launches a two-node Elasticsearch service with Kibana

You can expand the load-balanced configuration by:

1. Replicating the **es02** configuration as, e.g., **es03**
1. Adding each new node to the **discovery.seed_hosts** and **cluster.initial_master_nodes** properties
1. Adding a new data volume for each new node (e.g. **data03** for **es03**)

These configurations are adapted directly from Elastic's documentation, which includes the load-balanced configuration with the extra
**es03** node, so you can use the [Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker) section as a reference.

## Launching Elasticsearch

To launch Elasticsearch using the single-node configuration, run the command:

```bash
docker-compose up --detach
```

To launch Elasticsearch using the clustered configuration, run the command:

```bash
docker-compose --file docker-compose-es-cluster.yml up --detach
```

# Sending data to Elasticsearch

You need to configure any XNAT nodes that you want to submit data to your Elasticsearch service. The easiest way to do this is to install [Filebeat](https://www.elastic.co/products/beats/filebeat) on each XNAT node, then configure it to send to the appropriate Elasticsearch service address and port. [filebeat.yml](filebeat.yml) is a sample Filebeat configuration file. You can read more about [installing](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html) and [configuring](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-configuration.html) Filebeat on Elastic's site.

You can also check out the [configure-filebeat](../../../scripts/bin/configure-filebeat) script, which makes it relatively easy to install and configure Filebeat on a Vagrant VM provisioned using the [XNAT Vagrant project](../../../README.md):

```bash
$ configure-filebeat --server takeshi --nossl --username xnat
Please enter the password or token for the Elastic server:

Filebeat should now be installed and functioning. You can check the state of your Filebeat service
with the following commands:

 $ systemctl status filebeat.service
 $ journalctl --unit=filebeat.service
 $ journalctl --follow --unit=filebeat.service
 ```
